class ProductController < ApplicationController
  def new
    @product = Product.new
  end

  def create
    @product = Product.new(name: params[:product][:name],price: params[:product][:name])
    @product.save
    redirect_to top_main_path
  end

  def destroy
    Product.find(params[:id]).destroy
    redirect_to top_main_path
  end
end
