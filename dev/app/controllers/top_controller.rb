class TopController < ApplicationController
    def main
        if session[:login_uid] then
            render "home"
        else
            render"login"
        end
    end
    def login
        user = User.find_by(uid: params[:uid],pass: params[:pass])
        if user then 
            session[:login_uid] = params[:uid]
            redirect_to "/"
        else 
            render "error"
        end
    end
    def logout
        session.delete(:login_uid)
        redirect_to "/"
    end
end
