class UserController < ApplicationController
  
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
 
    if @user.save
      flash[:success] = '新規のユーザーを登録しました。'
      redirect_to @user
    else
      flash.now[:danger] = 'ユーザーの登録に失敗しました。'
      render :new
    end
  end
end
