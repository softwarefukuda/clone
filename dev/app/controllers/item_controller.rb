class ItemController < ApplicationController
  def create
    logger.debug "ID:"
    logger.debug params[:id]
    item = Item.find_by(product_id: params[:id],trade_id: session[:trade_id])
    if item
      item.qty += 1
    else
      item = Item.new(qty:1,product_id: params[:id], trade_id: session[:trade_id])
    end
    item.save
    redirect_to trade_show_path
  end

  def destroy
    Item.find(params[:id]).destroy
    redirect_to trade_show_path
  end
end

