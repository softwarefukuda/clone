class User < ActiveRecord::Base
     before_save { self.email.downcase! }
 
  validates :nickname, presence: true, length: { maximum: 50 }
 
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i },
                    uniqueness: { case_sensitive: false }
    private
 
  def user_params
    params.require(:user).permit(:nickname, :email, :password, :password_confirmation)
  end
end
