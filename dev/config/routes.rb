Rails.application.routes.draw do
  get 'user/index'

  get 'user/show'

  get 'user/new'

  get 'user/create'

  get 'user/edit'

  get 'user/update'

  resources :product,only: [:new,:create,:destroy]
  resources :item,only: [:create,:destroy]
  resources :users, only: [:index, :show, :create, :edit, :update]
  
  get 'signup', to: 'users#new'
  get 'trade/show'
  root 'top#main'
  get 'top/main'
  post 'top/login'
  get 'login/new'
  get 'trade/end'
  get 'top/error'
end

